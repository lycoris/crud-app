package com.wszib.crudapp.recipe;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wszib.crudapp.recipe.dto.RecipeDto;
import com.wszib.crudapp.recipe.model.Recipe;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("memory")
@TestClassOrder(ClassOrderer.OrderAnnotation.class)
@WithMockUser(roles = {"ADMIN"})
public class RecipeControllerIntegTest {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private MockMvc mockMvc;

    @Nested
    @Order(1)
    class GetTest {
        @Test
        public void testThatGetReturnsExpectedResponseAndRecipes() throws Exception {
            mockMvc.perform(get("/api/recipes")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.length()", is(3)))
                    .andExpect(jsonPath("$[1].recipeName", is("Roladki z kurczaka z kapustą")));
        }
    }

    @Nested
    @Order(2)
    class GetByIdTests {

        @Test
        public void testThatGetByIdReturnsExpectedRecipeWhenIdExists() throws Exception {
            mockMvc.perform(get("/api/recipes/1")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.recipeName", is("Żeberka w kapuście kiszonej")));
        }

        @Test
        public void testThatGetByIdReturnsNotFoundStatusWhenIdDoesntExists() throws Exception {
            mockMvc.perform(get("/api/recipes/101")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNotFound());
        }
    }

    @Nested
    @Order(3)
    class PostTest {

        @Test
        public void testThatPostSavesRecipeSuccessfully() throws Exception {
            MvcResult mvcResult = mockMvc.perform(post("/api/recipes")
                            .content(asJson(new RecipeDto(null, "test", "test", "test")))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated())
                    .andReturn();

            Recipe recipe = asRecipe(mvcResult.getResponse().getContentAsString());

            mockMvc.perform(get("/api/recipes/" + recipe.getId())
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.recipeName", is("test")));
        }

        @Test
        public void testThatPostReturnsBadRequestWhenRecipeContentDoesntPassValidation() throws Exception {
            mockMvc.perform(post("/api/recipes")
                            .content(asJson(new RecipeDto(null, "a/b", "test", "test")))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest());
        }
    }

    @Nested
    @Order(4)
    class PatchTest {
        @Test
        public void testThatPatchUpdatesRecipe() throws Exception {
            long testId = 4;

            saveTestRecipe();
            String testRecipeName = "patchTest";

            mockMvc.perform(patch("/api/recipes/" + testId)
                            .contentType("application/json-patch+json")
                            .content("{\"recipeName\": \"" + testRecipeName + "\"}"))
                    .andExpect(status().isOk());

            mockMvc.perform(get("/api/recipes/" + testId)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.recipeName", is(testRecipeName)));

        }

        @Test
        public void testThatPatchReturnStatusNotFoundWhenRecipeDoesntExists() throws Exception {
            String testRecipeName = "patchTest";

            mockMvc.perform(patch("/api/recipes/" + 2002)
                            .contentType("application/json-patch+json")
                            .content("{\"recipeName\": \"" + testRecipeName + "\"}"))
                    .andExpect(status().isNotFound());
        }
    }

    @Nested
    @Order(5)
    class DeleteTest {
        @Test
        public void testThatDeleteFinishesSuccessfullyWhenResourceDeleted() throws Exception {
            long nextId = 4L;

            saveTestRecipe();

            mockMvc.perform(delete("/api/recipes/" + nextId))
                    .andExpect(status().is2xxSuccessful());
        }

        @Test
        public void testThatDeleteReturnsNotFoundStatusWhenRecipeDoesntExistsInTheRepository() throws Exception {
            mockMvc.perform(delete("/api/recipes/10001"))
                    .andExpect(status().isNotFound());

        }

    }

    private void saveTestRecipe() throws Exception {
        mockMvc.perform(post("/api/recipes")
                        .content(asJson(new RecipeDto(null, "test", "test", "test")))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    public static Recipe asRecipe(String jsonRecipe) throws JsonProcessingException {
        return new ObjectMapper().readValue(jsonRecipe, Recipe.class);
    }

    public static String asJson(RecipeDto recipeDto) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(recipeDto);
    }
}
