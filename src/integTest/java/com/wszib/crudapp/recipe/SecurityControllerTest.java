package com.wszib.crudapp.recipe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wszib.crudapp.recipe.dto.RecipeDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("memory")
public class SecurityControllerTest {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testThatApiIsSecured() throws Exception {
        mockMvc.perform(get("/api/property?name=spring.profiles.active")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @ParameterizedTest
    @MethodSource("provideModifyingRequests")
    @WithMockUser(roles = {"USER"})
    public void testThatUserCannotAccessModifyingEndpoints(RequestBuilder requestBuilder) throws Exception {
        mockMvc.perform(requestBuilder)
                .andExpect(status().isForbidden());
    }

    @ParameterizedTest
    @MethodSource("provideGetRequests")
    @WithMockUser(roles = {"USER"})
    public void testThatUserCanAccessGetEndpoints(RequestBuilder requestBuilder) throws Exception {
        mockMvc.perform(requestBuilder)
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithMockUser(roles = {"TEST"})
    public void testThatOnlyAdminOrUserRolesCanAccessResources() throws Exception {
        mockMvc.perform(get("/api/property?name=spring.profiles.active")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    private static Stream<Arguments> provideGetRequests() {
        return Stream.of(
                Arguments.of(get("/api/property?name=spring.profiles.active")
                        .contentType(MediaType.APPLICATION_JSON)),
                Arguments.of(get("/api/recipes")
                        .contentType(MediaType.APPLICATION_JSON)),
                Arguments.of(get("/api/recipes/1")
                        .contentType(MediaType.APPLICATION_JSON))

        );
    }

    public static Stream<Arguments> provideModifyingRequests() throws JsonProcessingException {
        return Stream.of(
                Arguments.of(post("/api/recipes")
                        .content(asJson(new RecipeDto(null, "test", "test", "test")))
                        .contentType(MediaType.APPLICATION_JSON)),
                Arguments.of(patch("/api/recipes/1")
                        .contentType("application/json-patch+json")
                        .content("{\"recipeName\": \"test\"}")),
                Arguments.of(delete("/api/recipes/1"))
        );
    }

    public static String asJson(RecipeDto recipeDto) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(recipeDto);
    }
}
