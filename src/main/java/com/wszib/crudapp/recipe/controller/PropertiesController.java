package com.wszib.crudapp.recipe.controller;

import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("api/property")
public class PropertiesController {

    private final Environment env;

    public PropertiesController(Environment env) {
        this.env = env;
    }

    @GetMapping()
    public ResponseEntity<String> get(@RequestParam(name = "name") String propertyName) {
        return ResponseEntity.of(getPropertyFromEnv(propertyName));
    }

    private Optional<String> getPropertyFromEnv(String propertyName) {
        if (env.containsProperty(propertyName)) {
            return Optional.of(env.getProperty(propertyName));
        } else {
            return Optional.empty();
        }
    }
}
