package com.wszib.crudapp.recipe.controller;

import com.wszib.crudapp.recipe.dto.RecipeDto;
import com.wszib.crudapp.recipe.exception.RecipeNotFoundException;
import com.wszib.crudapp.recipe.model.Recipe;
import com.wszib.crudapp.recipe.service.RecipeService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/recipes")
public class RecipeController {
    private final RecipeService recipeService;

    public RecipeController(RecipeService service) {
        this.recipeService = service;
    }

    @GetMapping
    public ResponseEntity<List<Recipe>> getAll() {
        List<Recipe> recipes = recipeService.findAll();

        return ResponseEntity.ok().body(recipes);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Recipe> get(@PathVariable("id") Long id) {
        Optional<Recipe> recipe = recipeService.find(id);
        return ResponseEntity.of(recipe);
    }

    @PostMapping
    public ResponseEntity<Recipe> create(@Valid @RequestBody RecipeDto recipe) {
        Recipe createdRecipe = recipeService.create(recipe);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdRecipe.getId())
                .toUri();

        return ResponseEntity.created(location).body(createdRecipe);
    }

    @PatchMapping(value = "/{id}", consumes = "application/json-patch+json")
    public ResponseEntity<Recipe> update(
            @PathVariable("id") Long id,
            @RequestBody RecipeDto patch) {
        try {
            Recipe updatedRecipe = recipeService.update(id, patch);
            return ResponseEntity.ok(updatedRecipe);
        } catch (RecipeNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        try {
            recipeService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (RecipeNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
