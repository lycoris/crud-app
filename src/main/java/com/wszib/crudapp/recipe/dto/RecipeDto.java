package com.wszib.crudapp.recipe.dto;

import jakarta.validation.constraints.Pattern;
import org.springframework.validation.annotation.Validated;

@Validated
public class RecipeDto {
    public static final String RECIPE_NAME_PATTERN_VIOLATION = "Recipe name can contain only letters (upper and lower case), ampersands and apostrophes";
    public static final String INGREDIENTS_PATTERN_VIOLATION = "Each ingredient needs to be comma separated and can't end with comma";
    public static final String DESCRIPTION_PATTERN_VIOLATION = "Description must be string";

    private final Long id;
    @Pattern(regexp = "^[a-zA-ZąćęłńóśźżĆŁŚŹŻ &']+$", message = RECIPE_NAME_PATTERN_VIOLATION)
    private final String recipeName;

    @Pattern(regexp = "^[a-zA-Z0-9ąćęłńóśźżĆŁŚŹŻ ,]+[^,]$", message = INGREDIENTS_PATTERN_VIOLATION)
    private final String ingredients;

    @Pattern(regexp = "^[a-zA-Z0-9ąćęłńóśźżĆŁŚŹŻ ,.-]+", message = DESCRIPTION_PATTERN_VIOLATION)
    private final String description;

    public RecipeDto(Long id, String recipeName, String ingredients, String description) {
        this.id = id;
        this.recipeName = recipeName;
        this.ingredients = ingredients;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public String getIngredients() {
        return ingredients;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "RecipeDto{" +
                "id=" + id +
                ", recipeName='" + recipeName + '\'' +
                ", ingredients='" + ingredients + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
