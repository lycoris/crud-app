package com.wszib.crudapp.recipe.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.TableGenerator;

import java.util.Objects;

@Table(name = "recipes")
@Entity
public class Recipe {
    public static final String RECIPE_NAME_NULL_MESSAGE = "recipe name cannot be null";
    public static final String INGREDIENTS_NULL_MESSAGE = "ingredients list cannot be null";
    public static final String DESCRIPTION_NULL_MESSAGE = "recipe description cannot be null";

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "recipe_gen")
    @TableGenerator(name = "recipe_gen", table = "gen", initialValue = 3)
    private Long id;

    @Column(name = "recipe_name")
    private String recipeName;

    @Column(name = "ingredients")
    private String ingredients;

    @Column(name = "description", length = 10000)
    private String description;

    public Recipe() {

    }

    public Recipe(Long id, String recipeName, String ingredients, String description) {
        this.id = id;
        this.recipeName = Objects.requireNonNull(recipeName, RECIPE_NAME_NULL_MESSAGE);
        this.ingredients = Objects.requireNonNull(ingredients, INGREDIENTS_NULL_MESSAGE);
        this.description = Objects.requireNonNull(description, DESCRIPTION_NULL_MESSAGE);
    }

    public Long getId() {
        return id;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public String getIngredients() {
        return ingredients;
    }

    public String getDescription() {
        return description;
    }


    public Recipe updateWith(Recipe recipe) {
        return new Recipe(
                this.id,
                recipe.recipeName,
                recipe.ingredients,
                recipe.description
        );
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", recipeName='" + recipeName + '\'' +
                ", ingredients='" + ingredients + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
