package com.wszib.crudapp.recipe.repository;

import com.wszib.crudapp.recipe.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecipeRepository extends JpaRepository<Recipe, Long> {
}
