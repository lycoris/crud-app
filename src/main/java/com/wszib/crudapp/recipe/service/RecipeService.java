package com.wszib.crudapp.recipe.service;

import com.wszib.crudapp.recipe.dto.RecipeDto;
import com.wszib.crudapp.recipe.model.Recipe;

import java.util.List;
import java.util.Optional;

public interface RecipeService {
    List<Recipe> findAll();

    Optional<Recipe> find(Long id);

    Recipe create(RecipeDto recipe);

    Recipe update(Long recipeId, RecipeDto patch);

    void delete(Long id);
}
