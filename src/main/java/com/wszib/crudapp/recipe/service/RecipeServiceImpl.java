package com.wszib.crudapp.recipe.service;

import com.wszib.crudapp.recipe.dto.RecipeDto;
import com.wszib.crudapp.recipe.exception.RecipeNotFoundException;
import com.wszib.crudapp.recipe.model.Recipe;
import com.wszib.crudapp.recipe.repository.RecipeRepository;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RecipeServiceImpl implements RecipeService {
    private static final Logger LOG = LoggerFactory.getLogger(RecipeService.class);
    private final RecipeRepository recipeRepository;

    public RecipeServiceImpl(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public List<Recipe> findAll() {
        LOG.debug("Received request to fetch all recipes");
        return recipeRepository.findAll();
    }

    @Override
    public Optional<Recipe> find(Long id) {
        LOG.debug("Received request to fetch recipe with id [{}]", id);
        return recipeRepository.findById(id);
    }

    @Override
    public Recipe create(RecipeDto recipe) {
        LOG.debug("Received request to create new recipe entry {}", recipe);

        Recipe copy = new Recipe(
                null,
                recipe.getRecipeName(),
                recipe.getIngredients(),
                recipe.getDescription()
        );
        return recipeRepository.save(copy);
    }

    @Override
    public Recipe update(Long recipeId, RecipeDto patch) {
        LOG.debug("Received request to update recipe with id [{}] with values: {}", recipeId, patch);

        Optional<Recipe> recipe = recipeRepository.findById(recipeId);
        if (recipe.isPresent()) {
            Recipe updated = applyPatch(patch, recipe.get());
            return recipeRepository.save(updated);
        } else {
            throw new RecipeNotFoundException(String.format("Recipe with id %s doesnt exists", recipeId));
        }
    }

    @Override
    public void delete(Long id) {
        LOG.debug("Received request to delete recipe with id [{}]", id);
        if (recipeRepository.existsById(id)) {
            recipeRepository.deleteById(id);
        } else {
            LOG.debug("Recipe doesnt exist in the repository. Nothing to delete");
            throw new RecipeNotFoundException();
        }
    }

    private static Recipe applyPatch(RecipeDto patch, Recipe recipe) {
        return new Recipe(recipe.getId(),
                determineValue(patch.getRecipeName(), recipe.getRecipeName()),
                determineValue(patch.getIngredients(), recipe.getIngredients()),
                determineValue(patch.getDescription(), recipe.getDescription())
        );
    }

    private static String determineValue(String patch, String old) {
        return patch == null ? old : patch;
    }
}
