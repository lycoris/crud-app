
INSERT INTO recipes (id, recipe_name, ingredients, description)
VALUES (1, 'Żeberka w kapuście kiszonej',
        '1kg żeberek, 1kg kaputy kiszonej, 2 cebule, 1 marchewka, 6 pieczarek, olej, 1 szklanka bulionu, liście laurowe, gałązki rozmarynu, ziele angielskie, majeranek, sól, pieprz',
        'Kapustę przepłucz w zimnej wodzie i odciśnij, a następnie pokrój na mniejsze kawałki. ' ||
        'Marchewkę oskrob i zetrzyj na tarce. Pieczarki umyj, a następnie pokrój na plasterki. Posiekaj cebulę. ' ||
        'Żeberka podziel na mniejsze kawałki oprósz solą i pieprzem a następnie usmaż na oleju. ' ||
        'Gdy żeberka będą już zarumienione przełóż je do formy, w której całe danie upieczesz w piekarniku. ' ||
        'Na oleju, w którym smażyły się żeberka usmaż wcześniej posiekaną cebulę. Po chwili dodaj pieczarki ' ||
        'marchewkę oraz kapustę. Dodaj majeranek, ziele angielskie, liście laurowe, sól oraz pieprz. ' ||
        'Smaż wszystko 10 minut co jakiś czas mieszając. Na koniec do kapusty wlej szklankę bulionu ' ||
        'lub jeżeli nie masz wody. Piecz całość około 1,5h w 170 stopniach. Po upieczeniu udekoruj danie rozmarynem. ');
INSERT INTO recipes (id, recipe_name, ingredients, description)
VALUES (2, 'Roladki z kurczaka z kapustą',
        '1kg kapusty kiszonej, 1kg piersi z kurczaka, 100g wędzonego boczku, 1 cebula, 6 śliwek suszonych, olej, sól, pieprz, gałązki rozmarynu',
        'Kapustę przepłucz w zimnej wodzie, odciśnij, a następnie pokrój na mniejsze kawałki. ' ||
        'Pierś kurczaka pokrój na mniejsze filety tak żeby dało się w nie zawinać kapustę. Każdy filecik dodatkowo rozbij tłuczkiem. ' ||
        'Cebulę posiekaj. Rozgrzej olej na patelni, a następnie wrzuć tam cebulę i podsmaż przez chwilę. ' ||
        'Gdy cebula się zeszkli dodaj drobno pokrojony boczek. Gdy boczek jest już lekko podsmażony dodaj kapustę,' ||
        'drobno pokrojone śliwki i duś razem przez 10 minut. Gdyby danie za bardzo przywierało dodaj olej. ' ||
        'Przypraw do smaku solą, pieprzem oraz dodaj tymianek. Zacznij nadziewać filety z kurczaka.' ||
        'Farszem, który został wyłóż dno naczynia do pieczenia. Całość piecz 25minut w temperaturze 160 stopni.');
INSERT INTO recipes (id, recipe_name, ingredients, description)
VALUES (3, 'Ciasto francuskie z kapustą kiszoną',
        '1 opakowanie ciasta francuskiego, 800g kapusty kiszonej, 1 papryka, 1 cebula, 100g boczku wędzonego, ziele angielskie, sól, pieprz, kminek, 1 białko',
        'Paprykę umyj, oczyść z nasion i pokrój z kostke. Cebulę drobno posiekaj. Kapustę przepłucz w zimnej wodzie, ' ||
        'odciśnij, a następnie pokrój na mniejsze kawałki. Wrzuć paprykę oraz kapustę do garnka i zalej wodą tak żeby zakrywała kapustę. ' ||
        'Dodaj ziele angielskie, kminek.Gotuj całosć 25min. Odcedź i wystudź. Dopraw do smaku solą i pieprzem. ' ||
        'Na patelni rozgrzej olej, podsmaż cebulkę, a następnie, gdy cebula się zeszkli dodaj boczek. ' ||
        'Farsz z patelni wymieszaj razem z kapustą. Rozłóż ciasto francuskie na blacie, rozwałkuj niedoskonałości. ' ||
        'Wyłóż na ciasto równomiernie farsz, zostaw 2 cm marginesu z każdej strony. Gdy farsz jest już wyłożony zwiń ciasto w roladę. ' ||
        'Wyłóż blachę do pieczenia papierem ściernym. Ułóż na nim ciasto z farszem. Posmaruj z wierchu białkiem. ' ||
        'Piecz 25minut w 200 stopniach.');