package com.wszib.crudapp;

import com.wszib.crudapp.recipe.dto.RecipeDto;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


class RecipeDtoTest {
    private static final long TEST_ID = 1L;
    private static final String TEST_NAME = "name";
    private static final String TEST_INGREDIENTS = "a,b";
    private static final String TEST_DESCRIPTION = "description";
    private Validator validator;

    @BeforeEach
    public void setUp() {
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            validator = factory.getValidator();
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"Potatoes O'Brien", "Mac & Cheese"})
    public void testThatRecipeNameCanContainAllowedSpecialCharacters(String recipeName) {
        RecipeDto testRecipe = new RecipeDto(TEST_ID, recipeName, TEST_INGREDIENTS, TEST_DESCRIPTION);

        Set<ConstraintViolation<RecipeDto>> violationSet = validator.validate(testRecipe);
        assertThat(violationSet).isEmpty();

        assertThat(testRecipe.getRecipeName()).isEqualTo(recipeName);
    }

    @Test
    public void testThatRecipeWithForbiddenCharacterCannotBeCreated() {
        RecipeDto testRecipe = new RecipeDto(TEST_ID, "a/b", TEST_INGREDIENTS, TEST_DESCRIPTION);

        Set<ConstraintViolation<RecipeDto>> violationSet = validator.validate(testRecipe);
        assertThat(violationSet).isNotEmpty();
        assertThat(violationSet.size()).isEqualTo(1);
        assertThat(violationSet.iterator().next().getMessage()).isEqualTo(RecipeDto.RECIPE_NAME_PATTERN_VIOLATION);
    }

    @Test
    public void testThatRecipeWithOneIngredientCanBeCreated() {
        RecipeDto testRecipe = new RecipeDto(TEST_ID, TEST_NAME, "1 ingredient", TEST_DESCRIPTION);

        Set<ConstraintViolation<RecipeDto>> violationSet = validator.validate(testRecipe);
        assertThat(violationSet).isEmpty();
    }

    @Test
    public void testThatRecipeCanHaveMultipleIngredients() {
        RecipeDto testRecipe = new RecipeDto(TEST_ID, TEST_NAME, "1 ingredient, 2 ingredients, 3 ingredients", TEST_DESCRIPTION);

        Set<ConstraintViolation<RecipeDto>> violationSet = validator.validate(testRecipe);
        assertThat(violationSet).isEmpty();
    }

    @Test
    public void testThatRecipeIngredientsListCannotEndWithComma() {
        RecipeDto testRecipe = new RecipeDto(TEST_ID, TEST_NAME, "1 ingredient, 2 ingredients,", TEST_DESCRIPTION);

        Set<ConstraintViolation<RecipeDto>> violationSet = validator.validate(testRecipe);
        assertThat(violationSet).isNotEmpty();
        assertThat(violationSet.size()).isEqualTo(1);
        assertThat(violationSet.iterator().next().getMessage()).isEqualTo(RecipeDto.INGREDIENTS_PATTERN_VIOLATION);
    }

    @Test
    public void testThatIngredientsListCantHaveOtherSpecialCharactersThanComma() {
        RecipeDto testRecipe = new RecipeDto(TEST_ID, TEST_NAME, "1 ingredient; 2 ingredients", TEST_DESCRIPTION);

        Set<ConstraintViolation<RecipeDto>> violationSet = validator.validate(testRecipe);
        assertThat(violationSet).isNotEmpty();
        assertThat(violationSet.size()).isEqualTo(1);
        assertThat(violationSet.iterator().next().getMessage()).isEqualTo(RecipeDto.INGREDIENTS_PATTERN_VIOLATION);
    }

    @Test
    public void testThatExemplaryRecipePassesValidations() {
        RecipeDto testRecipe = new RecipeDto(TEST_ID, "Żeberka w kapuście kiszonej",
                "1kg żeberek, 1kg kaputy kiszonej, 2 cebule, 1 marchewka, 6 pieczarek, olej, 1 szklanka bulionu, liście laurowe, gałązki rozmarynu, ziele angielskie, majeranek, sól, pieprz",
                "Kapustę przepłucz w zimnej wodzie i odciśnij, a następnie pokrój na mniejsze kawałki." +
                        "Marchewkę oskrob i zetrzyj na tarce. Pieczarki umyj, a następnie pokrój na plasterki." +
                        "Posiekaj cebulę. Żeberka podziel na mniejsze kawałki oprósz solą i pieprzem a następnie usmaż na oleju." +
                        "Gdy żeberka będą już zarumienione przełóż je do formy, w której całe danie upieczesz w piekarniku." +
                        "Na oleju, w którym smażyły się żeberka usmaż wcześniej posiekaną cebulę. Po chwili dodaj pieczarki" +
                        "marchewkę oraz kapustę. Dodaj majeranek, ziele angielskie, liście laurowe, sól oraz pieprz." +
                        "Smaż wszystko 10 minut co jakiś czas mieszając. Na koniec do kapusty wlej szklankę bulionu" +
                        "lub jeżeli nie masz wody. Piecz całość około 1,5h w 170 stopniach. Po upieczeniu udekoruj danie rozmarynem."
        );

        Set<ConstraintViolation<RecipeDto>> violationSet = validator.validate(testRecipe);
        assertThat(violationSet).isEmpty();
    }

}