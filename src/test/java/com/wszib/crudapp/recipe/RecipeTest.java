package com.wszib.crudapp.recipe;

import com.wszib.crudapp.recipe.model.Recipe;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


class RecipeTest {
    private static final long TEST_ID = 1L;
    private static final String TEST_NAME = "name";
    private static final String TEST_INGREDIENTS = "a,b";
    private static final String TEST_DESCRIPTION = "description";

    @ParameterizedTest
    @MethodSource("provideRecipeBuildValues")
    public void testThatRecipeCannotBeCreatedWithNullParameter
            (Long id, String name, String ingredients, String description, String expectedMessage) {
        Exception exception = Assertions.assertThrows(NullPointerException.class,
                () -> new Recipe(id, name, ingredients, description));

        assertThat(exception.getMessage()).isEqualTo(expectedMessage);
    }

    private static Stream<Arguments> provideRecipeBuildValues() {
        return Stream.of(
                Arguments.of(TEST_ID, null, TEST_INGREDIENTS, TEST_DESCRIPTION, Recipe.RECIPE_NAME_NULL_MESSAGE),
                Arguments.of(TEST_ID, TEST_NAME, null, TEST_DESCRIPTION, Recipe.INGREDIENTS_NULL_MESSAGE),
                Arguments.of(TEST_ID, TEST_NAME, TEST_INGREDIENTS, null, Recipe.DESCRIPTION_NULL_MESSAGE)
        );
    }
}